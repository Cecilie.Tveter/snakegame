# Snake 

Dette spillet er en morsom og utfordrende variant av det klassiske Snake-spillet, som først ble introdusert i 1970 og 
ble populært på 90-tallet. I spillet tar du kontroll over en sulten slange som må navigere seg rundt på et 
todimensjonalt brett og spise røde epler som tilfeldig dukker opp. Jo flere epler slangen spiser, jo større vil den 
vokse seg - men pass på, for å klare dette må du unngå å krasje i kantene av brettet eller med slanges egen kropp. 
Med hvert eple spist vil spillet bli vanskeligere og utfordringene større, helt til slangen endelig vil krasje og det
hele vil være over. Er du klar for å ta på deg denne spennende utfordringen?


Link til video for demonstrasjon: https://youtu.be/1KsYIGqYjus