#Test

## Test for CheckFoodCollision
I denne filen har jeg valgt å teste checkFoodCollision manuelt. Årsaken til at jeg velger
å teste denne metoden manuelt er fordi jeg syns at det var vanskelig å teste denne metoden 
gjennom JUnit testene. Jeg har skrevet tester for denne metoden, men selv om plasseringen på 
eplet er ved hodet til slangen vil testen returnere false.

For å teste metoden kan man gjøre følgende:

1. Sette opp spillet med brettet med rader og kolonner, plassere slangen og eple et sted på brettet.
2. Kalle på metoden checkFoodCollision().
3. Hvis slangens hodet kolliderer med eplet bør metoden returnere true og øke parameteren 'appleEaten'
hver gang hodet treffer eplet. 
4. Dersom hodet ikke kolliderer med eplet, metoden skal returnere false og ikke endre på parameteren 
'appleEaten'. Vi bør sjekke om disse om disse påstandene holder. 
5. Vi bør teste dette 2-4 ganger for å sjekke om slangen spiser eplet og parameteren 'appleEaten' økes. 


