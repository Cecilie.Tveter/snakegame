package no.uib.inf101.sem2.View;

import no.uib.inf101.sem2.Grid.GridCell;
import no.uib.inf101.sem2.Grid.GridDimension;
import no.uib.inf101.sem2.Model.SnakeBoard;

public interface SnakeViewable {


    /**
     * Returns the dimension of the grid.
     * @return the dimension of the grid as a {@code GridDimension} object
     */
    GridDimension getDimension();

}
