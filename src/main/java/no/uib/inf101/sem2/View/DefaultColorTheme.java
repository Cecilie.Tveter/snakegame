package no.uib.inf101.sem2.View;

import java.awt.*;

public class DefaultColorTheme implements ColorTheme{

    @Override
    public Color getFrameColor() {
        return Color.black;
    }

    @Override
    public Color getBackgroundColor() {
        return Color.black;
    }
}
