package no.uib.inf101.sem2.View;

import java.awt.*;

public interface ColorTheme {

    Color getFrameColor ();

    Color getBackgroundColor ();
}
