package no.uib.inf101.sem2.View;

import no.uib.inf101.sem2.Grid.CellPosition;
import no.uib.inf101.sem2.Model.*;

import javax.swing.*;
import java.awt.*;
import java.awt.geom.Rectangle2D;

public class GameBoardView extends JPanel {
    private final ColorTheme colorTheme;
    private Snake snake;
    private SnakeBoard board;
    private GameModel snakeModel;
    int cellSize = 25;

    public GameBoardView(Snake snake, SnakeBoard board, GameModel model) {
        this.board = board;
        this.snakeModel = model;
        this.snake = snake;
        this.colorTheme = new DefaultColorTheme();
        this.setBackground(colorTheme.getBackgroundColor());
        this.setFocusable(true);
        this.setPreferredSize(new Dimension(500, 500));
}



    @Override
    public void paintComponent(Graphics g) {
        super.paintComponent(g);
        Graphics2D g2 = (Graphics2D) g;

        g2.setColor(Color.black);
        g2.fillRect(0, 0, getWidth(), getHeight());

        if (snakeModel.getGameState() == GameState.GAME_OVER){
            gameOver(g);
            scorePanel(g);
        }
        else {
            drawGame(g2);
            scorePanel(g2);
            drawSnake(g2);
            drawApple(g2);

    }
    }

    /**

     Draws the snake on the graphics object using the green color for each of its cells.
     @param g The graphics object to draw on.
     */
    public void drawSnake(Graphics g) {
        for (CellPosition cellPosition : snake.getBody()) {
            g.setColor(Color.GREEN);
            g.fillRect(cellPosition.col() * cellSize, cellPosition.row() * cellSize, cellSize, cellSize);

        }
    }

    /**
     Draws the apple on the graphics object using the red color for the oval shape.
     @param g The graphics object to draw on.
     */
    public void drawApple (Graphics g) {
        g.setColor(Color.RED);
        g.fillOval(snakeModel.getApple().getAppPos().col() * cellSize, snakeModel.getApple().getAppPos().row() * cellSize, board.rows(), board.cols());
    }

    /**
     Draws the game board and its components on the graphics object. This includes the frame, the snake, and the apple.
     @param g The graphics object to draw on.
     */
    public void drawGame(Graphics2D g) {
        double margin = 2;
        double x = margin;
        double y = margin;
        double width = this.getWidth() - 2 * margin;
        double height = this.getHeight() - 2 * margin;

        Rectangle2D rect = new Rectangle2D.Double(x, y, width, height);

        g.setColor(colorTheme.getFrameColor());
        g.fill(rect);

    }
    /**
     Draws the "Game Over" text in red font on the graphics object when the game is over.
     @param g The graphics object to draw on.
     */
    public void gameOver (Graphics g) {
        g.setColor(Color.red);
        g.setFont(new Font ("Ink Free", Font.BOLD, 75));
        FontMetrics metrics = getFontMetrics(g.getFont());
        int y = (getHeight() - metrics.getHeight()) / 2 + metrics.getAscent();
        int x = (getWidth() - metrics.stringWidth("Game Over")) / 2;
        g.drawString("Game Over",x, y);
    }
    /**
     Draws the score panel showing the player's score in red font on the graphics object.
     @param g The graphics object to draw on.
     */
    public void scorePanel(Graphics g) {
        g.setColor(Color.red);
        g.setFont( new Font("Ink Free",Font.BOLD, 40));
        FontMetrics metrics1 = getFontMetrics(g.getFont());
        g.drawString("Score: "+ snakeModel.appleEaten, (getWidth() - metrics1.stringWidth("Score: "+snakeModel.appleEaten))/2, g.getFont().getSize());
    }
    }



