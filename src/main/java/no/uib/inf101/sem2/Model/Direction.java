package no.uib.inf101.sem2.Model;

/**
 * This class represent the different directions that
 * the snake can move in.
 */

public enum Direction {
    UP, DOWN, LEFT, RIGHT
}
