package no.uib.inf101.sem2.Model;

import no.uib.inf101.sem2.Grid.CellPosition;
import no.uib.inf101.sem2.Grid.Grid;
import no.uib.inf101.sem2.Grid.GridDimension;
import no.uib.inf101.sem2.View.SnakeViewable;

/**
 * This class handles the game logic. It contains methods for
 * updating the game state and handling collisions.
 */
public class GameModel implements SnakeViewable {
    private SnakeBoard snakeBoard;
    private Snake snake;
    private Apple apple;
    private GameState gameScreen = GameState.ACTIVE_GAME;
    public int appleEaten = 0;

    public GameModel(SnakeBoard snakeBoard) {
        this.snakeBoard = snakeBoard;
        this.snake = new Snake(snakeBoard);
        this.apple = new Apple(snakeBoard.rows(), snakeBoard.cols());
    }

    /**
     Returns the current snake object.
     @return the current snake object
     */
    public Snake getSnake() {
        return snake;
    }


    @Override
    public GridDimension getDimension() {
        return new Grid<>(snakeBoard.rows(), snakeBoard.cols());
    }

    /**
     * This method checks if the snake collides with the
     * boarders of the game.
     * @return Returns true if the snake dosent collide.
     * Returns false if the snake collides with the boardes.
     */
    public boolean checkWallCollision() {
        CellPosition head = snake.getBody().get(0);
        int x = head.col();
        int y = head.row();

        if (x < 0 || x >= this.snakeBoard.cols()
                || y < 0 || y >= this.snakeBoard.rows()) {
            return true;
        }
        return false;
    }

    /**
     Checks if the snake's head collides with the apple.
     @return true if the snake's head collides with the apple, false otherwise
     */
    public boolean checkFoodCollision() {
        CellPosition head = getSnake().getBody().get(0);
        if (((head.col() == apple.getAppPos().col() && head.row() == apple.getAppPos().row()))) {
            appleEaten++;
            apple.newApple();
            return true;
            }
        return false;
    }

    /**
     Checks if the snake's head collides with any other part of its body.
     @return true if the snake's head collides with any other part of its body, false otherwise
     */
    public boolean checkSelfCollision() {
        CellPosition head = snake.getBody().get(0);
        for (int i = 1; i < snake.getBody().size(); i++) {
            if (head.col() == snake.getBody().get(i).col() && head.row() == snake.getBody().get(i).row()) {
                return true;
            }
        }
        return false;
    }

    /**
     * Sets the current game screen state.
     * @param gameScreen the game screen state to set
     */
    public void setGameScreen(GameState gameScreen) {
        this.gameScreen = gameScreen;
    }

    /**
     * Returns the current game screen state.
     * @return the current game screen state
     */
    public GameState getGameState () {
        return gameScreen;
    }

    /**
     * Returns the current apple object.
     * @return the current apple object.
     */
    public Apple getApple() {
        return apple;
    }
}
