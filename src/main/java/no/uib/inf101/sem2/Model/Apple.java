package no.uib.inf101.sem2.Model;

import no.uib.inf101.sem2.Grid.CellPosition;
import java.util.Random;

/**
 * Represents an apple in the game grid. The apple has a position in the grid and can be moved to a new position.
 */
public class Apple {
    private final Random random = new Random();
    public CellPosition appPos;
    private int maxRow;
    private int maxCol;

    /**
     * Creates a new Apple object and generates a new position for the apple.
     * @param maxRow the maximum row of the game grid
     * @param maxCol the maximum column of the game grid
     */
    public Apple(int maxRow, int maxCol) {
        this.maxRow = maxRow;
        this.maxCol = maxCol;
        newApple();
    }

    public void setAppPos(CellPosition appPos) {
        this.appPos = appPos;
    }

    /**
     * Returns the position of the current apple in the game.
     * @return the position of the current apple
     */
    public CellPosition getAppPos() {
        return appPos;
    }


    /**
     * Placing a new apple random on the board.
     */
    public void newApple() {
            CellPosition updateApple = new CellPosition((random.nextInt(maxRow)),
                    random.nextInt(maxCol));
            appPos = updateApple;
    }
}

