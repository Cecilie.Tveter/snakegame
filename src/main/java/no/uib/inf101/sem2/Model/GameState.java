package no.uib.inf101.sem2.Model;

/**
 * This class is an enum and keeps track of the current
 * state of the game.
 */

public enum GameState {
    ACTIVE_GAME, GAME_OVER
}
