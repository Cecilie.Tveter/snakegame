package no.uib.inf101.sem2.Model;

import no.uib.inf101.sem2.Grid.CellPosition;
import no.uib.inf101.sem2.View.SnakeViewable;

import java.util.ArrayList;


/**
 * This class represent the snake itself. It
 * handle's the snake position, length and direction.
 */

public class Snake {
    private ArrayList<CellPosition> body;
    private Direction direction;
    private SnakeBoard board;

    public Snake(SnakeBoard board) {
        this.board = board;
        this.direction = Direction.RIGHT;
        this.body = new ArrayList<>();
        body.add(new CellPosition(0, 1)); // Setter lokasjonen til snaken i punktet 0, 0.
        body.add(new CellPosition(1, 1));
    }
    /**
     * Gets the body of the snake
     *
     * @return Returns the body of the snake
     */
    public ArrayList<CellPosition> getBody() {
        return body;
    }

    /**
     Moves the snake by updating the positions of all its segments. The head moves in the direction determined by
     the current direction, and each subsequent segment is moved to the position of the segment that preceded it.
     */
    public void move() {
        CellPosition head = body.get(0);
        for (int i = body.size() -1; i > 0; i--) {
            CellPosition prevSegmnet = body.get(i - 1);
            body.set(i, prevSegmnet);
        }

        switch (direction) {
            case UP:
                head = new CellPosition(head.col(), head.row() - 1);
                break;
            case DOWN:
                head = new CellPosition(head.col(), head.row() + 1);
                break;
            case LEFT:
                head = new CellPosition(head.col() - 1, head.row());
                break;
            case RIGHT:
                head = new CellPosition(head.col() + 1, head.row());
                break;
            default:
                throw new IllegalArgumentException();
        }
        body.set(0, new CellPosition(head.col(), head.row()));
    }
    /**
     * When a snake eats an apple the body of the snake will grow with
     * one part.
     */
    public void grow() {
        CellPosition lastPart = body.get(body.size() - 1);
        body.add(new CellPosition(lastPart.col(), lastPart.row() ));
    }
    /**
     Returns the current direction of the snake.
     @return the current direction of the snake
     */
    public Direction getDirection() {
        return direction;
    }
    /**
     Sets the direction of the snake.
     @param direction the direction to be set for the snake
     */
    public void setDirection(Direction direction) {
        this.direction = direction;
    }

}



