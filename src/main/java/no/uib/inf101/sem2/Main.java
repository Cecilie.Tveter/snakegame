package no.uib.inf101.sem2;

import no.uib.inf101.sem2.Controller.Controller;
import no.uib.inf101.sem2.Model.Snake;
import no.uib.inf101.sem2.Model.SnakeBoard;
import no.uib.inf101.sem2.Model.GameModel;
import no.uib.inf101.sem2.View.GameBoardView;

import javax.swing.JFrame;

public class Main {
  public static void main(String[] args) {

    SnakeBoard snakeBoard = new SnakeBoard(20, 20);
    Snake snake = new Snake(snakeBoard);
    GameModel snakeModel = new GameModel(snakeBoard);
    GameBoardView view = new GameBoardView(snakeModel.getSnake(), snakeBoard, snakeModel);
    Controller controller = new Controller(view, snakeModel.getSnake(), snakeModel);

    JFrame frame = new JFrame();
    frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    frame.setTitle("Snake");
    frame.setContentPane(view);
    frame.pack();
    frame.setVisible(true);
    frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);


  }
}
