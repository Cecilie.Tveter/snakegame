package no.uib.inf101.sem2.Controller;

import no.uib.inf101.sem2.Model.*;
import no.uib.inf101.sem2.View.GameBoardView;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;

public class Controller implements KeyListener, ActionListener {
    static final int DELAY = 100;
    GameBoardView snakeView;
    Snake snake;
    public Timer timer;
    GameModel snakeModel;
    GameState gameState;


    public Controller(GameBoardView snakeView, Snake snake, GameModel snakeModel) {
        this.snake = snake;
        this.snakeView = snakeView;
        this.snakeModel = snakeModel;
        snakeView.addKeyListener(this);
        snakeView.setFocusable(true);
        snakeView.requestFocus();
        startGame();
    }

    /**
     Starts the game timer.
     Creates a new Timer object with the delay specified in the DELAY constant and starts it.
     */
    public void startGame() {
        timer = new Timer(DELAY, this);
        timer.start();
    }

    @Override
    public void keyTyped(KeyEvent e) {
    }

    @Override
    public void keyPressed(KeyEvent e) {
        switch (e.getKeyCode()) {
            case KeyEvent.VK_LEFT:
                if (snake.getDirection() != Direction.RIGHT) {
                    snake.setDirection(Direction.LEFT);
                    break;
                }
                break;
            case KeyEvent.VK_RIGHT:
                if (snake.getDirection() != Direction.LEFT) {
                    snake.setDirection(Direction.RIGHT);
                    break;
                }
            case KeyEvent.VK_UP:
                if (snake.getDirection() != Direction.DOWN) {
                    snake.setDirection(Direction.UP);
                    break;
                }
            case KeyEvent.VK_DOWN:
                if (snake.getDirection() != Direction.UP) {
                    snake.setDirection(Direction.DOWN);
                    break;
                }
        }
        snakeView.repaint();
    }

    @Override
    public void keyReleased(KeyEvent e) {
        if(getGameScreen() == GameState.GAME_OVER) {
            snakeView.repaint();
        }

    }

    @Override
    public void actionPerformed(ActionEvent e) {
        //if (getGameScreen() == GameState.ACTIVE_GAME) {
        if (getGameScreen() == GameState.ACTIVE_GAME) {
            snake.move();
            if (snakeModel.checkWallCollision()) {
                timer.stop();
                this.gameState = GameState.GAME_OVER;
            } else if (snakeModel.checkFoodCollision()) {
                snakeModel.getSnake().grow();
            } else if (snakeModel.checkSelfCollision()){
                timer.stop();
                this.gameState = GameState.GAME_OVER;
            }
        }
        snakeView.repaint();
        snakeModel.setGameScreen(gameState);

    }

    /**
     * Returns the current state of the game screen.
     * @return the current state of the game screen
     */
    public GameState getGameScreen() {
        return GameState.ACTIVE_GAME;
}

}


