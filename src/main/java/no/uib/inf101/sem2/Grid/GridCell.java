package no.uib.inf101.sem2.Grid;
/**
 A record representing a cell in a grid, consisting of a position and a value.
 @param <E> the type of the value in the cell.
 */
public record GridCell <E> (CellPosition pos, E value) {
}
