package no.uib.inf101.sem2.Grid;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class Grid<E> implements  IGrid<E> {
    private final List<List<E>> grid;
    private final int rows, cols;


    public Grid (int rows, int cols) {
        this(rows, cols, null);
    }

    public Grid(int rows, int cols, E initialvalue) {
        this.rows = rows;
        this.cols = cols;
        this.grid = new ArrayList<>();


        for (int row = 0; row < rows; row++) {
            List<E> allRows = new ArrayList<>();
            for (int col = 0; col < cols; col++) {
                allRows.add(initialvalue);
            }
            this.grid.add(allRows);
        }
    }

    @Override
    public void set(CellPosition pos, E value) {
        this.grid.get(pos.col()).set(pos.row(), value);
    }

    @Override
    public E get(CellPosition pos) {
        if (!positionIsOnGrid(pos)) {
            throw new IndexOutOfBoundsException();
        }
        return this.grid.get(pos.col()).get(pos.row());
    }

    @Override
    public boolean positionIsOnGrid(CellPosition pos) {
        return pos.col() >= 0 && pos.col() < rows() && pos.row() >= 0 && pos.row() < cols();
    }

    @Override
    public Iterator iterator() {
        ArrayList<GridCell<E>> gridCells = new ArrayList<>();
        for (int row = 0; row < rows; row++) {
            for (int col = 0; col < cols; col++) {
                CellPosition cellPosition = new CellPosition(row, col);
                E item = get(cellPosition);
                GridCell<E> gridCell = new GridCell<>(cellPosition, item);
                gridCells.add(gridCell);
            }
        }
        return gridCells.iterator();
    }


    @Override
    public int rows() {
        return rows;
    }

    @Override
    public int cols() {
        return cols;
    }
}
