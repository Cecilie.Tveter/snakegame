package no.uib.inf101.sem2.Grid;
/**
 This interface specifies the dimensions of a grid, which consists of rows and columns.
 */
public interface GridDimension {
    /**
     Returns the number of rows in the grid.
     @return the number of rows in the grid
     */
    int rows();
    /**
     Returns the number of columns in the grid.
     @return the number of columns in the grid
     */
    int cols();
}
