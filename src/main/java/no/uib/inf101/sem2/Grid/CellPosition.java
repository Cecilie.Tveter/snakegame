package no.uib.inf101.sem2.Grid;


/**
 A class representing a position in a grid, with a column and row.
 The position is immutable, meaning it cannot be changed once created.
 @param col the column index of the position
 @param row the row index of the position
 */
public record CellPosition (int col, int row) {
}
