package no.uib.inf101.sem2.Model;

import no.uib.inf101.sem2.Grid.CellPosition;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class SnakeModelTest {

    @Test
    void shouldCollideWithEdge() {
        SnakeBoard board = new SnakeBoard(10, 10);
        GameModel model = new GameModel(board);

        // Test snake at the edge of board
        model.getSnake().getBody().set(0, new CellPosition(10, 10));
        assertEquals(true, model.checkWallCollision());
    }

    @Test
    void placingSnakeOnboard() {
        SnakeBoard board = new SnakeBoard(10, 10);
        GameModel model = new GameModel(board);

        // Test snake inside the board
        model.getSnake().getBody().set(0, new CellPosition(5, 5));
        assertEquals(false, model.checkWallCollision());

    }

    @Test
    void testWallCollisionOutside() {
        SnakeBoard board = new SnakeBoard(10, 10);
        GameModel model = new GameModel(board);

        // Test snake outside the board
        model.getSnake().getBody().set(0, new CellPosition(-1, 5));
        assertEquals(true, model.checkWallCollision());
    }


    @Test
    void testFoodCollision() {
        SnakeBoard board = new SnakeBoard(10, 10);
        GameModel model = new GameModel(board);
        Apple apple = new Apple(10, 10);

        // Add a few cells to the snake's body
        model.getSnake().getBody().add(new CellPosition(0, 0));
        model.getSnake().getBody().add(new CellPosition(0, 1));
        model.getSnake().getBody().add(new CellPosition(0, 2));

        // Place the apple in the same position as the snake's head
        apple.setAppPos(new CellPosition(0, 2));

        // Move the snake's head away from the apple and call the method again
        model.getSnake().getBody().remove(0);
        model.getSnake().getBody().add(new CellPosition(1, 2));

        assertFalse(model.checkFoodCollision());

    }

    @Test
    void testSelfCollision() {
        SnakeBoard board = new SnakeBoard(10, 10);
        GameModel model = new GameModel(board);

        // Add a few cells to the snake's body
        model.getSnake().getBody().add(new CellPosition(5, 5));
        model.getSnake().getBody().add(new CellPosition(5, 4));
        model.getSnake().getBody().add(new CellPosition(5, 3));
        // Set the snake's head to collide with its own body
        model.getSnake().getBody().set(0, new CellPosition(5, 3));

        assertEquals(true, model.checkSelfCollision());
    }
}