package no.uib.inf101.sem2.Model;

import no.uib.inf101.sem2.Grid.CellPosition;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class SnakeTest {

    @Test
    void shouldSnakeMove() {
        SnakeBoard board = new SnakeBoard(10, 10);
        GameModel model = new GameModel(board);

        model.getSnake().getBody().add(new CellPosition(0, 0));
        model.getSnake().getBody().add(new CellPosition(0, 1));


        model.getSnake().move();

        assertEquals(new CellPosition(1, 1), model.getSnake().getBody().get(0));
        assertEquals(new CellPosition(0, 1), model.getSnake().getBody().get(1));

    }
}